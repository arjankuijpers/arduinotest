// common.h

#ifndef _COMMON_h
#define _COMMON_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "wprogram.h"
#else
	#include "WProgram.h"
#endif





#define BIT_1	1
#define BIT_2	(1 << 1)
#define BIT_3	(1 << 2)
#define BIT_4	(1 << 3)
#define BIT_5	(1 << 4)
#define BIT_6	(1 << 5)
#define BIT_7	(1 << 6)
#define BIT_8	(1 << 7)
#define BIT_9	(1 << 8)
#define BIT_10	(1 << 9)
#define BIT_11	(1 << 10)
#define BIT_12	(1 << 11)
#define BIT_13	(1 << 12)
#define BIT_14	(1 << 13)
#define BIT_15	(1 << 14)
#define BIT_16	(1 << 15)
#define BIT_17	(1 << 16)
#define BIT_18	(1 << 17)
#define BIT_19	(1 << 18)
#define BIT_20	(1 << 19)
#define BIT_21	(1 << 20)
#define BIT_22	(1 << 21)
#define BIT_23	(1 << 22)
#define BIT_24	(1 << 23)
#define BIT_25	(1 << 24)
#define BIT_26	(1 << 25)
#define BIT_27	(1 << 26)
#define BIT_28	(1 << 27)
#define BIT_29	(1 << 28)
#define BIT_30	(1 << 29)
#define BIT_31	(1 << 30)
#define BIT_32	(1 << 31)

class BitMask
{
public:


	void SetFlag(int flag);
	void UnsetFlag(int flag);

	bool IsSet(int flag);
	bool hasAny(int flag);

	int Get() { return m_mask; };
	void SetInt(int value = 0) { m_mask = value; };
	void ToggleFlag(int flag) { m_mask ^= flag; }

	BitMask(unsigned int mask) : m_mask(mask) {}
	BitMask& operator = (int flag) { this->m_mask = flag; return *this; };
private:

	unsigned int m_mask = 0;
};






struct LedFSetting
{
	unsigned int intervalMSLow = 1000;
	unsigned int intervalMSHight = 1000;
};

struct LedData
{
	unsigned int intervalMSLow = 2800;
	unsigned int intervalMSHigh = 200;

	volatile int ledState = LOW;
	long previousTime = 0;
};


struct SystemTimer
{
	float deltaTime = 0;
	float previousTime = millis();
	float currentTime = millis();
};

struct Timers
{
	float ledTimer = 0;
	float sensorUpdateTimer = 0;
	float gameSendTimer = 0;

	float reconnectTimer = 0;
	float lostTimer = 0;
	float errorTimer = 0;
};

enum EDeviceState
{
	DEVICE_STATE_ERROR = 0,
	DEVICE_STATE_BOOT = 1,
	DEVICE_STATE_IDLE_SERIAL_OPEN,
	DEVICE_STATE_IDLE_SERIAL_CLOSED,
	DEVICE_STATE_DEVICE_CONNECTED_GAME,
	DEVICE_STATE_DEVICE_GAME_SENDING_DATA,
	DEVICE_STATE_DEVICE_GAME_RECONNECT,
	DEVICE_STATE_GAME_CONNECTION_LOST,

};

struct DeviceState
{
	EDeviceState currentState;
	EDeviceState previousState;
	BitMask additionalInfo = 0;
	bool SerialOpen;
};


struct AxisData
{
	union AXIS
	{
		struct XY
		{
			short x;
			short y;
		} xy;
		
		// xy as an integer.
		int ixy;
	} axis;
	

};



// Data struct for Unreal.

struct BCHeader
{
	int magicnumber;
	int dataSize; // size of the actual data without the command. that is also included in body.

};


struct BCBody
{
	int command;
	unsigned char data[52];
};


struct BCFrame
{
	BCHeader header;
	BCBody body;
};



// Commands

enum BCCommands
{
	COMMAND_DEVICE_RESTART = 0,  // UMETA(DisplayName = "Restart Device"),
	COMMAND_DEVICE_CONNECT = 1,  // UMETA(DisplayName = "Connect Device"),

	COMMAND_GAME_START = 2,  // UMETA(DisplayName = "start game mode"),
	COMMAND_GAME_STOP = 3,   // UMETA(DisplayName = "Stop game mode"),
	COMMAND_GAME_RECONNECT = 4,  // UMETA(DisplayName = "Game reconnect"),
	COMMAND_GAME_SEND_DATA = 5,  // UMETA(DisplayName = "Send game sensor data"),
	COMMAND_GAME_RECEIVE_DATA = 6,  // UMETA(DisplayName = "Receive game sensor data"),

	COMMAND_COMMON_SET_LIGHT_DG = 7,  // UMETA(DisplayName = "Set light digital"),
	COMMAND_COMMON_SET_LIGHT_AN = 8,  //  UMETA(DisplayName = "Set light analog"),

	COMMAND_COMMON_REQUEST_DG = 11,  // UMETA(DisplayName = "Get Digital value (RQ)"),
	COMMAND_COMMON_RECEIVE_DG = 12,  // UMETA(DisplayName = "Get Digital value (RC)"),
	COMMAND_COMMON_REQUEST_AN = 13,  // UMETA(DisplayName = "Get Analog value (RQ)"),
	COMMAND_COMMON_RECEIVE_AN = 14,  // UMETA(DisplayName = "Get Analog value (RC)"),

	COMMAND_COMMON_SET_DG = 15,  // UMETA(DisplayName = "Set Digital value (RQ)"),
	COMMAND_COMMON_SET_AN = 16,  // UMETA(DisplayName = "Set Analog value (RQ)")

};



#endif

