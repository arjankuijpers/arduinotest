/*
 Name:		ArduinoBico.ino
 Created:	11/25/2017 8:18:27 PM
 Author:    Arjan Kuijpers
*/

// Common used struct and functions.
#include "common.h"


#define MAGICNUMBERSTRING "bico"

//// CONST
#define CONST_AXIS_MIN_VALUE 0
#define CONST_AXIS_MAX_VALUE 1023


// Timers for systems.
// Milliseconds
#define TIMEOUT_SENSOR_UPDATE 8 // ms
#define TIMEOUT_GAME_SEND 16 //ms
#define TIMEOUT_ERROR 20000 // ms
#define TIMOUT_RECONNECT 5000 //ms
#define TIMEOUT_LOST 5000 //ms

///////////////////////////////////////////


//#define SERIAL_BAUDRATE 128000
#define SERIAL_BAUDRATE 9600

//////////////////////////////////

// Sensor logic.
///////////////////////////


#define PORT_AXIS_X 18
#define PORT_AXIS_Y 19

#define AXIS_BUFFER_COUNT 5

#define AXIS_SENSOR_BIAS 0
//#define AXIS_SENSOR_BIAS -512


/////////////////////////////

#define MAGIC_MACRO_INT (*(int*)(MAGICNUMBERSTRING))


////////////////////////////////////////


// Data

// Device state led information
LedFSetting lfsDeviceIdleBoot = { 2800, 400 };
LedFSetting lfsDeviceErrorState = { 1000, 1000 };

LedFSetting lfsDeviceSerialOpen = { 2000, 100 };
LedFSetting lfsDeviceConnectedGame = { 500, 150 };
LedFSetting lfsDeviceGameSensorSend = { 100, 100 };
LedFSetting lfsDeviceGameReconnect = { 100, 700 };
LedFSetting lfsDeviceGameLost = { 100, 2000 };
// End of Device state led information

// The port the device mode led is on, 13 is the port that is linked to onboard led of the Teensy.
const unsigned int portLedDeviceMode = 13;

//////////////////////////////////////////////////////////////////////////////////////////////

// System variables. DO NOT CHANGE.
LedData ledStateData;
DeviceState deviceState;
Timers timers;


// time.
SystemTimer st;

// End of System variables.

// End of Data.

//helper functions.
void SetLedBlinkMode(const LedFSetting& lfs)
{
	{
		ledStateData.intervalMSLow = lfs.intervalMSLow;
		ledStateData.intervalMSHigh = lfs.intervalMSHight;
	}
};
void SwitchDeviceState(EDeviceState newDeviceState)
{
	deviceState.previousState = deviceState.currentState;
	deviceState.currentState = newDeviceState;

	switch (deviceState.currentState)
	{
	case DEVICE_STATE_ERROR:
	{
		SetLedBlinkMode(lfsDeviceErrorState);
		break;
	}

	case DEVICE_STATE_BOOT:
	{
		SetLedBlinkMode(lfsDeviceIdleBoot);
	}

	case DEVICE_STATE_IDLE_SERIAL_OPEN:
	{
		SetLedBlinkMode(lfsDeviceSerialOpen);
		break;
	}
	case DEVICE_STATE_IDLE_SERIAL_CLOSED:
	{
		SetLedBlinkMode(lfsDeviceIdleBoot);
		break;
	}

	case DEVICE_STATE_DEVICE_CONNECTED_GAME:
	{
		SetLedBlinkMode(lfsDeviceConnectedGame);
		break;
	}

	case DEVICE_STATE_DEVICE_GAME_SENDING_DATA:
	{
		SetLedBlinkMode(lfsDeviceGameSensorSend);
		break;
	}

	case DEVICE_STATE_DEVICE_GAME_RECONNECT:
	{
		SetLedBlinkMode(lfsDeviceGameReconnect);
		break;
	}

	case DEVICE_STATE_GAME_CONNECTION_LOST:
	{
		SetLedBlinkMode(lfsDeviceGameLost);
		break;
	}
	}
}

// end of helper functions.

////////////////////////////////////////////////////////////////////////////////////
//
// Begin of Core.
//
////////////////////////////////////////////////////////////////////////////////////

// the setup function runs once when you press reset or power the board
void setup() {
	// setup teensy pins.
	// Settings pin modes to OUTPUT is more energy efficient.
	for (int i = 0; i < 21; i++) {
		pinMode(i, OUTPUT);
	}

	DeviceSetup();
	BlinkSetup();
	ButtonSetup();

	SerialComSetup();

	GameSensorSetup();
}

// Dont use this if you dont know what you are doing.
// This functions reset the device. all data and states get lost.
void(*__ResetController) (void) = 0;

// the loop function runs over and over again until power down or reset
void loop() {

	UpdateTime();

	GameSensorUpdate();

	SerialUpdate();
	DeviceUpdate();

	BlinkUpdate();
	ButtonUpdate();
	
	GameSendData();

	delay(1);
}


void UpdateTime()
{
	st.currentTime = millis();
	st.deltaTime = st.currentTime - st.previousTime;
	st.previousTime = st.currentTime;

}

////////////////////////////////////////////////////////////////////////////////////
//
// End of Core.
//
////////////////////////////////////////////////////////////////////////////////////

// Led Blink Logic
//////////////////////////////////

// setup of the device state led system
void BlinkSetup()
{
	pinMode(portLedDeviceMode, OUTPUT);
}
void BlinkUpdate()
{
	switch (ledStateData.ledState)
	{
	case LOW:
	{
		unsigned long currentTime = millis();
		if (currentTime - ledStateData.previousTime > ledStateData.intervalMSLow)
		{
			ledStateData.previousTime = currentTime;
			ledStateData.ledState = HIGH;
			digitalWrite(portLedDeviceMode, ledStateData.ledState);
		}
	}
	break;
	case HIGH:
	{
		unsigned long currentTime = millis();
		if (currentTime - ledStateData.previousTime > ledStateData.intervalMSHigh)
		{
			ledStateData.previousTime = currentTime;
			ledStateData.ledState = LOW;
			digitalWrite(portLedDeviceMode, ledStateData.ledState);
		}
	}
	break;
	default:
		break;
	}
}

// Button Logic
/////////////////////////////////////////////////

// Button setup,
void ButtonSetup()
{
	pinMode(20, OUTPUT);
	pinMode(2, INPUT);
}

// button update that checks the state every loop.
void ButtonUpdate()
{
	if (digitalRead(2))
	{
		digitalWrite(20, HIGH);
	}
	else
	{
		digitalWrite(20, LOW);
	}
}

// Serial Logic
//////////////////////////////////////////

// COM (Serial) Command based protocol with data payload.


void SerialComSetup()
{
	Serial.begin(SERIAL_BAUDRATE);
}

void SerialUpdate()
{
	if (!Serial || (!Serial.available() && !Serial.availableForWrite()))
	{
		deviceState.SerialOpen = false;
		return;
	}

	// else serial is open.
	deviceState.SerialOpen = true;
	ReadSerial();
}

void ReadSerial()
{

	if (Serial.available() > 0)
	{
		/*char buff[65];
		memset(&buff[0], 0, 65);
		Serial.readBytes(&buff[0], 64);
		buff[65] = '\0';
		
		Serial.write(buff, 64);*/
		
		////////////////////////////////////

		char buff[64];
		memset(&buff[0], 0, 64);
		Serial.readBytes(&buff[0], 64);

		BCFrame frame;
		memcpy(&frame, &buff[0], sizeof(BCFrame));

		ReadFrame(frame);
		
	}
	
}

// Device Functions.
// These are device state related functions.

void DeviceSetup()
{
	deviceState.additionalInfo = 0;
	deviceState.currentState = DEVICE_STATE_BOOT;
	deviceState.previousState = DEVICE_STATE_BOOT;
	SwitchDeviceState(DEVICE_STATE_BOOT);
	deviceState.SerialOpen = false;
}

void DeviceUpdate()
{
	switch (deviceState.currentState)
	{
	case DEVICE_STATE_ERROR:
	{


		if (timers.errorTimer <= 0)
		{
			timers.errorTimer = TIMEOUT_ERROR;

			// do logic

			__ResetController();
		}
		else
		{
			timers.errorTimer -= st.deltaTime;
		}
		
		break;
	}

	case DEVICE_STATE_BOOT:
	{
		if (deviceState.SerialOpen)
		{
			SwitchDeviceState(DEVICE_STATE_IDLE_SERIAL_OPEN);
		}
		break;
	}

	case DEVICE_STATE_IDLE_SERIAL_OPEN:
	{
		if (!deviceState.SerialOpen)
		{
			SwitchDeviceState(DEVICE_STATE_IDLE_SERIAL_CLOSED);
		}
		break;
	}
	case DEVICE_STATE_IDLE_SERIAL_CLOSED:
	{
		if (deviceState.SerialOpen)
		{
			SwitchDeviceState(DEVICE_STATE_IDLE_SERIAL_OPEN);
		}
		break;
	}

	case DEVICE_STATE_DEVICE_CONNECTED_GAME:
	{
		if (!deviceState.SerialOpen)
		{
			SwitchDeviceState(DEVICE_STATE_DEVICE_GAME_RECONNECT);
		}
		break;
	}

	case DEVICE_STATE_DEVICE_GAME_SENDING_DATA:
	{
		if (!deviceState.SerialOpen)
		{
			SwitchDeviceState(DEVICE_STATE_DEVICE_GAME_RECONNECT);
		}
		break;
	}

	case DEVICE_STATE_DEVICE_GAME_RECONNECT:
	{

		if (timers.reconnectTimer <= 0)
		{
			timers.reconnectTimer = TIMOUT_RECONNECT;

			// do logic

			SwitchDeviceState(DEVICE_STATE_GAME_CONNECTION_LOST);
		}
		else
		{
			timers.reconnectTimer -= st.deltaTime;
		}
		

		break;
	}

	case DEVICE_STATE_GAME_CONNECTION_LOST:
	{
		

		if (timers.lostTimer <= 0)
		{
			timers.lostTimer = TIMEOUT_LOST;

			// do logic
			SwitchDeviceState(DEVICE_STATE_IDLE_SERIAL_CLOSED);
		}
		else
		{
			timers.lostTimer -= st.deltaTime;
		}

		break;
	}

	default:
		SwitchDeviceState(DEVICE_STATE_ERROR);
		break;
	}
}

// Sensor logic.
///////////////////////////



// buffer that results are stored in.
AxisData axisSensorBuffer[AXIS_BUFFER_COUNT];
// keeps track of the position in ring buffer.
unsigned char currentBufferIndex; 

// Current frames raw axis read from sensor.
AxisData rawAxisFromSensor;
// You want to use this value its an average of the past [AXIS_BUFFER_COUNT] sensor frames. makes sure that signal spikes have lesser impact.
AxisData avarageAxis;
// Corrected average axis, corrects and changes the scale of the axis readings.
AxisData correctedAverageAxis;


void GameSensorSetup()
{
	pinMode(PORT_AXIS_X, INPUT);
	pinMode(PORT_AXIS_Y, INPUT);

	memset(&axisSensorBuffer, 0, sizeof(AxisData) * AXIS_BUFFER_COUNT);
	currentBufferIndex = 0;
	rawAxisFromSensor.axis.ixy = 0;
	avarageAxis.axis.ixy = 0;
	correctedAverageAxis.axis.ixy = 0;

}

void GameSensorUpdate()
{
	if (deviceState.currentState != DEVICE_STATE_DEVICE_GAME_SENDING_DATA)
	{
		return;
	}


	if (timers.sensorUpdateTimer <= 0)
	{
		timers.sensorUpdateTimer = TIMEOUT_SENSOR_UPDATE;
	}
	else
	{
		timers.sensorUpdateTimer -= st.deltaTime;
		return;
	}



	rawAxisFromSensor.axis.xy = { static_cast<short>(analogRead(PORT_AXIS_X)) , static_cast<short>(analogRead(PORT_AXIS_Y)) };
	axisSensorBuffer[currentBufferIndex % AXIS_BUFFER_COUNT] = rawAxisFromSensor;

	unsigned int axisXCb = 0;
	unsigned int axisYCb = 0;
	for (size_t i = 0; i < AXIS_BUFFER_COUNT; i++)
	{
		axisXCb += axisSensorBuffer[i].axis.xy.x;
		axisYCb += axisSensorBuffer[i].axis.xy.y;
	}

	avarageAxis.axis.xy.x = (axisXCb / AXIS_BUFFER_COUNT);
	avarageAxis.axis.xy.y = (axisYCb / AXIS_BUFFER_COUNT);

	
	correctedAverageAxis.axis.xy = { ((short)avarageAxis.axis.xy.x) + ((short)AXIS_SENSOR_BIAS), ((short)avarageAxis.axis.xy.y) + ((short)AXIS_SENSOR_BIAS) };

	currentBufferIndex++;

	
}


// Game Update
///////////////////////////////////////////////


void GameSendData()
{

	if (deviceState.currentState != DEVICE_STATE_DEVICE_GAME_SENDING_DATA)
	{
		return;
	}



	if (timers.gameSendTimer <= 0)
	{
		timers.gameSendTimer = TIMEOUT_GAME_SEND;

		// do logic
		WriteGameSensorFrame(correctedAverageAxis);

		
	}
	else
	{
		timers.gameSendTimer -= st.deltaTime;
	}
}

void WriteGameSensorFrame(AxisData axis)
{
	BCFrame frame;
	frame.header.magicnumber = MAGIC_MACRO_INT;
	frame.header.dataSize = sizeof(AxisData);

	frame.body.command = BCCommands::COMMAND_GAME_RECEIVE_DATA;
	memset(frame.body.data, 0, sizeof(char) * 52);

	memcpy(frame.body.data, &axis, sizeof(AxisData));
	Serial.write((char*)&frame, sizeof(BCFrame));
}





void ReadFrame(BCFrame frame)
{

	if (frame.header.magicnumber != MAGIC_MACRO_INT)
	{
		// this frame is invalid.
		return;
	}

	if (frame.body.command == (*((int*)"CONN")))
	{
		WriteHelloFrame();
		return;
	}

	
	SwitchCommands((BCCommands)frame.body.command, frame.body.data);


}

void WriteHelloFrame()
{
	BCFrame frame;
	frame.header.magicnumber = MAGIC_MACRO_INT;
	frame.header.dataSize = 0;

	frame.body.command = -1;
	memset(frame.body.data, 0, sizeof(char) * 52);
	Serial.write((char*)&frame, sizeof(BCFrame));
}


// data contains 52 bytes of data as a max.
void SwitchCommands(BCCommands commands, unsigned char* data)
{


	switch (commands)
	{
	case COMMAND_DEVICE_RESTART:
		break;
	case COMMAND_DEVICE_CONNECT:
		CommandDeviceConnect();
		break;
	case COMMAND_GAME_START:
		// the game is started.
		break;
	case COMMAND_GAME_STOP:
		// the game stopped.
		break;
	case COMMAND_GAME_RECONNECT:
		break;
	case COMMAND_GAME_SEND_DATA:
		SwitchDeviceState(EDeviceState::DEVICE_STATE_DEVICE_GAME_SENDING_DATA);
		break;
	case COMMAND_GAME_RECEIVE_DATA:
		break;
	case COMMAND_COMMON_SET_LIGHT_DG:
		break;
	case COMMAND_COMMON_SET_LIGHT_AN:
		break;
	case COMMAND_COMMON_REQUEST_DG:
		break;
	case COMMAND_COMMON_RECEIVE_DG:
		break;
	case COMMAND_COMMON_REQUEST_AN:
		break;
	case COMMAND_COMMON_RECEIVE_AN:
		break;
	case COMMAND_COMMON_SET_DG:
		break;
	case COMMAND_COMMON_SET_AN:
		break;
	default:
		break;
	}

}


void CommandDeviceConnect()
{
	BCFrame frame;
	frame.header.magicnumber = MAGIC_MACRO_INT;
	frame.header.dataSize = sizeof(int);
	frame.body.command = (int)COMMAND_DEVICE_CONNECT;

	memset(&frame.body.data, 0, sizeof(char) * 52);

	int* intPtr = (int*)("ACK0");
	memcpy(frame.body.data, intPtr, sizeof(int));
	Serial.write((char*)&frame, sizeof(BCFrame));

	SwitchDeviceState(EDeviceState::DEVICE_STATE_DEVICE_CONNECTED_GAME);
}