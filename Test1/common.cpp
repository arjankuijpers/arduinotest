// 
// 
// 

#include "common.h"

void BitMask::SetFlag(int flag)
{
	m_mask |= flag;
}

void BitMask::UnsetFlag(int flag)
{
	m_mask &= ~flag;
}

bool BitMask::IsSet(int flag)
{
	return ((m_mask & flag) == flag);
}

bool BitMask::hasAny(int flag)
{
	return ((m_mask & flag) != 0);
}
